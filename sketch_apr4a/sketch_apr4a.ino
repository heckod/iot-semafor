// Semafor 1
int r1 = 2;
int y1 = 3;
int g1 = 4;

// Semafor na přechodu
int redP = 8;
int greenP = 9;

// Tlačítko
int button = 10;

// Fotorezistor
int foto = A0;

bool buttonPressed = true;

void setup() {
  pinMode(r1, OUTPUT);
  pinMode(y1, OUTPUT);
  pinMode(g1, OUTPUT);

  pinMode(redP, OUTPUT);
  pinMode(greenP, OUTPUT);

  pinMode(button, INPUT_PULLUP);
  Serial.begin(9600);
}

void cyklus(){
  //cyklus semaforu
  digitalWrite(redP, HIGH);
    digitalWrite(g1, HIGH);
    delay(10000);
    digitalWrite(redP, LOW);
    digitalWrite(y1, HIGH);
    delay(1000);
    digitalWrite(g1, LOW);
    digitalWrite(y1, LOW);
    digitalWrite(greenP, HIGH);
    digitalWrite(r1, HIGH);
    delay(10000);
    digitalWrite(y1, HIGH);
    delay(1000);
    digitalWrite(y1, LOW);
    digitalWrite(greenP, LOW);
    digitalWrite(r1, LOW);
}

void loop() {
  int photoValue = analogRead(foto);
  Serial.println(photoValue +"\n");
  if (photoValue < 300) { //pokud hodnota menší než 300, blikají žluté světla
    for (int i = 0; i < 5; i++) {
      digitalWrite(y1, HIGH);
      delay(500);
      digitalWrite(y1, LOW);
      delay(500);
    }
    digitalWrite(redP, LOW);
    digitalWrite(greenP, LOW);
  } else {
    cyklus();

    if (digitalRead(button) == LOW && buttonPressed) { //pokud je tlacitko zmacknuto tak funguje pro chodce prechod
      if (digitalRead(button) == LOW && buttonPressed) { //rozbito, hodne jsem experimentoval s tim :/ takže to nefunguje
      digitalWrite(greenP, HIGH);
      digitalWrite(r1, HIGH);
      digitalWrite(y1, LOW);
      digitalWrite(g1,LOW);
      delay(5000);
      digitalWrite(greenP, LOW);   
      digitalWrite(r1, LOW);     
      buttonPressed = false;       
    }
  }
 }
}
