# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala | 5 hodin|
| jak se mi to podařilo rozplánovat | jel jsem čistý freestyle |
| návrh designu | V souboru|
| proč jsem zvolil tento design | byl nejjednodušší |
| zapojení | V souboru |
| z jakých součástí se zapojení skládá | rezistor, drát |
| realizace | V souboru |
| jaký materiál jsem použil a proč | železo, papír, lepidlo|
| co se mi povedlo | nic |
| co se mi nepovedlo/příště bych udělal/a jinak | asi bych určitě nedělal křižovatku :) |
| zhodnocení celé tvorby | davam tomu tak 6/10|